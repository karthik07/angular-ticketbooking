var app = angular.module('main', ['ngRoute']);

//configur router 
app.config(function ($routeProvider){
	$routeProvider
	.when('/',{
		templateUrl : 'pages/home.html',
		controller  : 'myController'
	})
	// route for the about page
    .when('/theater', {
        templateUrl : 'pages/theater.html',
        controller  : 'theaterController'
    })

    // route for the contact page
    .when('/movies', {
        templateUrl : 'pages/movies.html',
        controller  : 'moviesController'
    })

    .when('/book-ticket', {
        templateUrl : 'pages/book-ticket.html',
        controller  : 'bookTicketController'
    });
});


app.controller('myController',['$scope', '$location', function ($scope, $location){
	$scope.item = [
		{path: '/', title: 'Home'},
		{path: '/theater', title: 'Theater'},
		{path: '/movies', title: 'Movies'}
	];
	$scope.isActive = function(items) {
      if (items.path == $location.path()) {
        return true;
      }
      return false;
    };
}]);

app.controller('theaterController', ['$scope', '$http', function ($scope, $http) {
	
    //$scope.message = 'Theater';
    //init();
    $scope.init = function(){
    	$http.get('ajax/theater-master.php',{"getElement": "get_data"}).success( function (response){
		//console.log(response);
			$scope.theaters = response;
		});
    }
    //console.log($scope);
	//save form code;
	$scope.submitForm = function (){
		//console.log();
		var url = 'ajax/theater-master-save.php?action=add_theater';
		 $http.post(url, {'uname': this.theatername }
        ).success(function(data, status, headers, config) {
        	console.log(data);
            if (data.msg != '')
            {
                $scope.msgs = data.msg;
                $scope.init();
                $scope.cancelCreating();
            }
            else
            {
                $scope.errors = data.error;
                $scope.init();
            }
        }).error(function(data, status) { // called asynchronously if an error occurs
// or server returns response with an error status.
            $scope.errors.push(status);
        });
	}
	//get data for edit
	$scope.editing = function (index){
		//console.log(index);
		var url = 'ajax/theater-master-save.php?action=edit_theater';
		$http.post(url, {'id': index }
        ).success(function(data, status, headers, config) {
        	$scope.startEditing();
        	console.log(data.id);
        	$scope.theatername = data.name;
        	$scope.id = data.id;
        }).error(function(data, status) { // called asynchronously if an error occurs
// or server returns response with an error status.
            $scope.errors.push(status);
        });
	}

	//Editing master data
	$scope.submitEdit = function (){
		var url = 'ajax/theater-master-save.php?action=edit_theater_data';
		$http.post(url, {'id': this.id, 'name': this.theatername }
        ).success(function(data, status, headers, config) {
        	console.log(data);
        	$scope.cancelEditing();
        	$scope.init();
        }).error(function(data, status) { // called asynchronously if an error occurs
// or server returns response with an error status.
            $scope.errors.push(status);
        });
	}

	//deleting record 
	$scope.delete = function (index){
		var url = 'ajax/theater-master-save.php?action=delete_theater_data';
		$http.post(url, {'id': index }
        ).success(function(data, status, headers, config) {
        	console.log(data);
        	$scope.init();
        }).error(function(data, status) { // called asynchronously if an error occurs
// or server returns response with an error status.
            $scope.errors.push(status);
        });
	}

	$scope.isCreating = false;
	$scope.isEditing = false;

	$scope.startCreating = function (){
		$scope.isCreating = true;
		$scope.isEditing = false;
	}
	$scope.startEditing = function (){
		$scope.isCreating = false;
		$scope.isEditing = true;
	}
	$scope.cancelCreating = function(){
		$scope.isCreating = false;
	}
	$scope.cancelEditing = function(){
		$scope.isEditing = false;
	}

}]);

    app.controller('moviesController', ['$scope', '$http', function ($scope, $http) {
    $scope.message = 'Movies';
    //init();
    $scope.init = function(){
    	$http.get('ajax/movies-master.php?action=get_movies').success( function (response){
		//console.log(response);
			$scope.movies = response;
		});
		
    }

    //Get theater name value
    // $scope.getTheaterName = function(index){
    //     console.log(index);
    //     $http.get('ajax/movies-master.php?action=get_theater_name', {'id': index }).success( function (response){
    //     //console.log(response);
    //         return response;
    //     });
    // }

    $scope.submitForm = function (){
		//console.log();
		var url = 'ajax/movies-master.php?action=add_movies';
		var d1 = {'uname': this.theatername, 'moviename': this.moviename, 'date': this.date1, 'time': this.time1, 'price': this.price };
		console.log(this.date1);
        console.log('<br/>');
        console.log(d1);
		$http.post(url, d1
        ).success(function(data, status, headers, config) {
        	console.log(data);
        	console.log(data);
            if (data.msg != '')
            {
                $scope.msgs = data.msg;
                $scope.init();
                $scope.cancelCreating();
            }
            else
            {
                $scope.errors = data.error;
                $scope.init();
            }
        }).error(function(data, status) { // called asynchronously if an error occurs
// or server returns response with an error status.
            $scope.errors.push(status);
        });
	}
    //get theater name
    // function getTheaterName(index){
    //     console.log(index);
    //     var prop = "";
    //     $http.get('ajax/movies-master.php?action=get_theater_name', {'id': index }).success( function (response){
    //     //console.log(response);
    //         prop = response;
    //     });
    // }

	//editing movie
	$scope.editing = function (index){
		//console.log(index);
		var url = 'ajax/movies-master.php?action=edit_movie';
		$http.post(url, {'id': index }
        ).success(function(data, status, headers, config) {
        	$scope.startEditing();
        	console.log(data);
        	$scope.theatername = data.theater_id;
        	$scope.moviename = data.movie_name;
        	$scope.timing = data.timing;
        	$scope.price = data.price;
        	$scope.id = data.id;
        }).error(function(data, status) { // called asynchronously if an error occurs
// or server returns response with an error status.
            $scope.errors.push(status);
        });
	}

    //Editing master data
    $scope.submitEdit = function (){
        var url = 'ajax/movies-master.php?action=edit_movie_data';
        $http.post(url, {'id': this.id, 'uname': this.theatername, 'moviename': this.moviename, 'date': this.date, 'time': this.time, 'price': this.price }
        ).success(function(data, status, headers, config) {
            console.log(data);
            $scope.cancelEditing();
            $scope.init();
        }).error(function(data, status) { // called asynchronously if an error occurs
// or server returns response with an error status.
            $scope.errors.push(status);
        });
    }
    //deleting record 
    $scope.delete = function (index){
        var url = 'ajax/movies-master.php?action=delete_movies_data';
        $http.post(url, {'id': index }
        ).success(function(data, status, headers, config) {
            console.log(data);
            $scope.init();
        }).error(function(data, status) { // called asynchronously if an error occurs
// or server returns response with an error status.
            $scope.errors.push(status);
        });
    }



    $scope.isCreating = false;
	$scope.isEditing = false;

	$scope.startCreating = function (){
		$scope.isCreating = true;
		$scope.isEditing = false;
	}
	$scope.startEditing = function (){
		$scope.isCreating = false;
		$scope.isEditing = true;
	}
	$scope.cancelCreating = function(){
		$scope.isCreating = false;
	}
	$scope.cancelEditing = function(){
		$scope.isEditing = false;
	}
}]);

//Book ticket
app.controller('bookTicketController',['$scope', '$location', function ($scope, $location){
    $scope.ticketBooking = "Ticket booking";
}]);


// // create the module and name it scotchApp
//     var app = angular.module('main', []);

//     // create the controller and inject Angular's $scope
//     app.controller('myController', function($scope) {

//         // create a message to display in our view
//         $scope.message = 'Everyone come and see how good I look!';
//     });