# AngularJS Application with mysql and php

## Overview

This application takes the developer through the process of building a web-application using
angular with php and mysql. The basic operation of Add, Edit and Delete using angular. I have used ngRouter concept to page.  

## Workings of the application

- The application filesystem layout structure is based on the [angular-crud-operation] project.
- There is dynamic backend (PHP application server) for this application.

## use of applications
1. Clone the source.
2. Paste your source in appache server in htdocs.
3. Insert the angular.sql file in your phpmyadmin
3. Run your application. 
4. You can change name as you like, and do you add edit delete concept using angular. 


